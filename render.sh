#!/bin/bash

INPUT_FILE=$1

BLENDER_PATH="/home/kasm-user/blender/blender"
OUTPUT="/home/kasm-user/animation"

# Render the animation

$BLENDER_PATH -b $INPUT_FILE -o $OUTPUT/animation -a -x